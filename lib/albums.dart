import 'dart:convert';

import 'package:application_flutter_musique/routes.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AlbumPageWidgetState extends StatelessWidget {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final String arg;

  AlbumPageWidgetState({
    required this.arg,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Align(
              alignment: AlignmentDirectional(-1, 0),
              child: SingleChildScrollView(
                child: FutureBuilder(
                  future: getAlbumData(arg),
                  builder:
                      (BuildContext context, AsyncSnapshot<Album> snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:
                        // En attendant le résultat, affichons un loader.
                        return new Center(
                            child: new CircularProgressIndicator());
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          // En cas d'erreur.
                          print(snapshot.error);
                          return new Text(
                            '${snapshot.error}',
                            style: TextStyle(color: Colors.red),
                          );
                        } else {
                          return Column(
                            children: snapshot.data!.list.map((item) {
                              return SingleChildScrollView(
                                  child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                    Container(
                                      width: 100,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.4,
                                      decoration: BoxDecoration(
                                        color: Color(0xFFEEEEEE),
                                      ),
                                      child: Stack(
                                        children: [
                                          Align(
                                            alignment:
                                                AlignmentDirectional(0, 0),
                                            child: Image.network(
                                              item.thumbnail,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  1,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Align(
                                            alignment: AlignmentDirectional(
                                                -0.84, 0.76),
                                            child: Image.network(
                                              item.thumbnail,
                                              width: 125,
                                              height: 125,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Align(
                                            alignment: AlignmentDirectional(
                                                -0.04, -0.75),
                                            child: Text(
                                              item.artist,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 30,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment:
                                                AlignmentDirectional(0.10, 0.2),
                                            child: Text(
                                              item.artist,
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 30,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: AlignmentDirectional(
                                                0.08, 0.40),
                                            child: Text(
                                              'Région, Type',
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.blueGrey,
                                              ),
                                            ),
                                          ),
                                          Align(
                                            alignment: AlignmentDirectional(
                                                0.90, -0.75),
                                            child: IconButton(
                                              icon: Image.network(
                                                "icons/Like.svg",
                                                height: 100,
                                                fit: BoxFit.fill,
                                              ),
                                              onPressed: () async {
                                                print("like");
                                              },
                                            ),
                                          ),
                                          Align(
                                            alignment: AlignmentDirectional(
                                                -0.90, -0.75),
                                            child: IconButton(
                                              icon: Image.network(
                                                "assets/icons/Fleche_gauche.svg",
                                                height: 100,
                                                fit: BoxFit.fill,
                                              ),
                                              onPressed: () async {
                                                dynamic res =
                                                    await Navigator.of(context)
                                                        .pushNamed(
                                                  AppRoutes.routeRecherche,
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsetsDirectional.fromSTEB(
                                          10, 10, 10, 0),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            height: 50,
                                            decoration: BoxDecoration(
                                              color: Color(0xFFEEEEEE),
                                            ),
                                            child: Stack(
                                              children: [
                                                Align(
                                                  alignment:
                                                      AlignmentDirectional(
                                                          -0.89, 0.1),
                                                  child: Container(
                                                    width: 100,
                                                    height: 30,
                                                    decoration: BoxDecoration(
                                                      color: Colors.white,
                                                    ),
                                                    child: Row(
                                                      mainAxisSize:
                                                          MainAxisSize.max,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              EdgeInsetsDirectional
                                                                  .fromSTEB(0,
                                                                      0, 5, 0),
                                                          child: Text(
                                                            'Star',
                                                          ),
                                                        ),
                                                        Text(
                                                          item.score,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  alignment:
                                                      AlignmentDirectional(
                                                          -0.09, 0.13),
                                                  child: Text(
                                                    '${item.votes} votes',
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            "${item.description.substring(0, 175)}...",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.blueGrey),
                                          ),
                                          Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 20,
                                              ),
                                              Padding(
                                                padding: EdgeInsetsDirectional
                                                    .fromSTEB(0, 0, 0, 5),
                                                child: Text(
                                                  'Titres',
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 30,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.95,
                                                height: 1,
                                                decoration: BoxDecoration(
                                                  color: Colors.grey,
                                                ),
                                              ),
                                            ],
                                          ),
                                          FutureBuilder(
                                            future: getTrackList(item.id),
                                            builder: (BuildContext context,
                                                AsyncSnapshot<Tracks>
                                                    snapshot) {
                                              switch (
                                                  snapshot.connectionState) {
                                                case ConnectionState.waiting:
                                                  // En attendant le résultat, affichons un loader.
                                                  return new Center(
                                                      child:
                                                          new CircularProgressIndicator());
                                                case ConnectionState.done:
                                                  if (snapshot.hasError) {
                                                    // En cas d'erreur.
                                                    print(snapshot.error);
                                                    return new Text(
                                                      '${snapshot.error}',
                                                      style: TextStyle(
                                                          color: Colors.red),
                                                    );
                                                  } else {
                                                    return Column(
                                                        children: snapshot
                                                            .data!.list
                                                            .map((track) {
                                                      var index = snapshot
                                                              .data!.list
                                                              .indexOf(track) +
                                                          1;
                                                      return Title(
                                                        title: track.title,
                                                        index: index.toString(),
                                                      );
                                                    }).toList());
                                                  }
                                                default:
                                                  return new Text('');
                                              }
                                            },
                                          ),
                                        ],
                                      ),
                                    )
                                  ]));
                            }).toList(),
                          );
                        }
                      default:
                        return new Text('');
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Album {
  Album(
    this.list,
  );
  final List<AlbumElement> list;

  getAlbumList() {
    return list;
  }
}

class AlbumElement {
  AlbumElement(
    this.id,
    this.title,
    this.artist,
    this.year,
    this.style,
    this.thumbnail,
    this.description,
    this.score,
    this.votes,
  );

  final String id;
  final String title;
  final String artist;
  final String year;
  final String style;
  final String thumbnail;
  final String description;
  final String score;
  final String votes;

  AlbumElement.fromJson(Map<String, dynamic> json)
      : id = json["idAlbum"] == null ? null : json["idAlbum"],
        title = json["strAlbum"] == null ? null : json["strAlbum"],
        artist = json["strArtist"] == null ? null : json["strArtist"],
        year = json["intYearReleased"] == null ? null : json["intYearReleased"],
        style = json["strStyle"] == null ? null : json["strStyle"],
        thumbnail =
            json["strAlbumThumb"] == null ? null : json["strAlbumThumb"],
        description =
            json["strDescriptionEN"] == null ? null : json["strDescriptionEN"],
        score = json["intScore"] == null ? null : json["intScore"],
        votes = json["intScoreVotes"] == null ? null : json["intScoreVotes"];
}

Future<Album> getAlbumData(id) async {
  var url = Uri.parse("https://theaudiodb.com/api/v1/json/2/album.php?m=$id");
  var response = await http.get(url);
  Map albumMap = jsonDecode(response.body);
  List<AlbumElement> liste = [];
  for (int i = 0; i < albumMap["album"].length; i++) {
    liste.add(AlbumElement.fromJson(albumMap["album"][i]));
  }
  Album listAlbum = Album(liste);
  return listAlbum;
}

class Tracks {
  Tracks(
    this.list,
  );

  final List<Track> list;
  getTrackList() {
    return list;
  }
}

class Track {
  Track(
    this.title,
  );

  final String title;

  Track.fromJson(Map<String, dynamic> json)
      : title = json["strTrack"] == null ? null : json["strTrack"];
}

Future<Tracks> getTrackList(id) async {
  var url = Uri.parse("https://theaudiodb.com/api/v1/json/2/track.php?m=$id");
  var response = await http.get(url);
  Map trackMap = jsonDecode(response.body);
  List<Track> liste = [];
  for (int i = 0; i < trackMap["track"].length; i++) {
    liste.add(Track.fromJson(trackMap["track"][i]));
  }
  Tracks listTrack = Tracks(liste);
  return listTrack;
}

class Title extends StatelessWidget {
  final String title;
  final String index;
  const Title({required this.title, required this.index, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.95,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(5, 5, 20, 5),
                child: Text(
                  index,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(5, 5, 20, 5),
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 1,
            decoration: BoxDecoration(
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }
}
