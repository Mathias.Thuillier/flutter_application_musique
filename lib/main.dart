import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Application de musique',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      onGenerateRoute: AppRoutes.getRoutes,
    );
  }
}

class Screen1 extends StatelessWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Align(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.1,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 20, 0, 0),
                  child: Text(
                    'Classements',
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      fontFamily: 'SFProDisplay',
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 30,
                    ),
                  ),
                ),
              ),
            ),
            Align(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.8,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: DefaultTabController(
                  length: 2,
                  initialIndex: 0,
                  child: Column(
                    children: [
                      TabBar(
                        labelColor: Colors.black,
                        unselectedLabelColor: Color(0xFF5F5F5F),
                        indicatorColor: Colors.green,
                        tabs: [
                          Text(
                            'Titres',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontFamily: 'SFProDisplay',
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 15,
                            ),
                          ),
                          Text(
                            'Albums',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontFamily: 'SFProDisplay',
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 15,
                            ),
                          )
                        ],
                      ),
                      Expanded(
                        child: TabBarView(
                          children: [
                            new SingleChildScrollView(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  FutureBuilder(
                                    future: getSingles(
                                        'https://theaudiodb.com/api/v1/json/523532/trending.php?country=us&type=itunes&format=singles'),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<listSingles> snapshot) {
                                      switch (snapshot.connectionState) {
                                        case ConnectionState.waiting:
                                          // En attendant le résultat, affichons un loader.
                                          return new Center(
                                              child:
                                                  new CircularProgressIndicator());
                                        case ConnectionState.done:
                                          if (snapshot.hasError) {
                                            // En cas d'erreur.
                                            return new Text(
                                              '${snapshot.error}',
                                              style:
                                                  TextStyle(color: Colors.red),
                                            );
                                          } else {
                                            // Une fois le résultat obtenu, affichons la réponse obtenue.
                                            return Column(
                                              children: snapshot.data!.liste
                                                  .map((single) {
                                                return TitresElements(
                                                    image: single.image,
                                                    nom: single.nom,
                                                    titre: single.titre,
                                                    rang: single.rang);
                                              }).toList(),
                                            );
                                          }
                                          break;
                                        default:
                                          return new Text('');
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                            new SingleChildScrollView(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  FutureBuilder(
                                    future: getSingles(
                                        'https://theaudiodb.com/api/v1/json/523532/trending.php?country=us&type=itunes&format=albums'),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<listSingles> snapshot) {
                                      switch (snapshot.connectionState) {
                                        case ConnectionState.waiting:
                                          // En attendant le résultat, affichons un loader.
                                          return new Center(
                                              child:
                                                  new CircularProgressIndicator());
                                        case ConnectionState.done:
                                          if (snapshot.hasError) {
                                            // En cas d'erreur.
                                            return new Text(
                                              '${snapshot.error}',
                                              style:
                                                  TextStyle(color: Colors.red),
                                            );
                                          } else {
                                            // Une fois le résultat obtenu, affichons la réponse obtenue.
                                            return Column(
                                              children: snapshot.data!.liste
                                                  .map((single) {
                                                return TitresElements(
                                                    image: single.image,
                                                    nom: single.nom,
                                                    titre: single.titre,
                                                    rang: single.rang);
                                              }).toList(),
                                            );
                                          }
                                          break;
                                        default:
                                          return new Text(
                                              'Erreur du chargement');
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Align(child: new Menu(numero: 1)),
          ],
        ),
      ),
    );
  }
}

class listSingles {
  final List<single> liste;
  listSingles(this.liste);

  getList() {
    return this.liste;
  }

  addList(single) {
    this.liste.add(single);
  }
}

class single {
  final String image;
  final String titre;
  final String rang;
  final String nom;

  single(this.image, this.titre, this.rang, this.nom);

  single.fromJson(Map<dynamic, dynamic> json)
      : image = json["strTrackThumb"] == null
            ? json["strAlbumThumb"]
            : json["strTrackThumb"],
        rang = json["intChartPlace"],
        nom = json["strArtist"],
        titre = json["strTrack"] == null ? json["strAlbum"] : json["strTrack"];
}

Future<listSingles> getSingles(URL) async {
  // Construction de l'URL a appeler
  var url = Uri.parse(URL);
  // Appel
  var response = await http.get(url);
  // Convertit le json
  Map singlesMap = jsonDecode(response.body);
  // Mapper le json dans l'objet SunriseSunsetTimes
  List<single> liste = [];
  for (int i = 0; i < singlesMap["trending"].length; i++) {
    liste.add(single.fromJson(singlesMap["trending"][i]));
  }

  listSingles listee = new listSingles(liste);
  return listee;
}

class TitresElements extends StatelessWidget {
  final String image;
  final String nom;
  final String titre;
  final String rang;
  const TitresElements(
      {required this.image,
      Key? key,
      required this.nom,
      required this.titre,
      required this.rang})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.1,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Align(
                  alignment: AlignmentDirectional(0, 0),
                  child: Text('$rang',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'SFProDisplay',
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.2,
                height: 70,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    '$image',
                    width: MediaQuery.of(context).size.width * 0.5,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                      alignment: AlignmentDirectional(-0.85, 0),
                      child: Text('$nom',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontFamily: 'SFProDisplay',
                          )),
                    ),
                    Align(
                      alignment: AlignmentDirectional(-0.85, 0),
                      child: Text('$titre',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontFamily: 'SFProDisplay',
                          )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class Menu extends StatelessWidget {
  final int numero;
  const Menu({required this.numero, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.1,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              IconButton(
                icon: Image.network(
                  "icons/Accueil_classements.svg",
                  height: 100,
                  fit: BoxFit.fill,
                  color: numero == 1
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                ),
                onPressed: () async {
                  if (numero != 1) {
                    dynamic res = await Navigator.of(context).pushNamed(
                      AppRoutes.routeHome,
                    );
                  }
                },
              ),
              Text(
                'Classements',
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontFamily: 'SFProDisplay',
                  fontWeight: FontWeight.bold,
                  color: numero == 1
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                  fontSize: 12,
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              IconButton(
                icon: Image.network(
                  "icons/Recherche_Loupe.svg",
                  height: 100,
                  fit: BoxFit.fill,
                  color: numero == 2
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                ),
                onPressed: () async {
                  if (numero != 2) {
                    dynamic res = await Navigator.of(context).pushNamed(
                      AppRoutes.routeRecherche,
                    );
                  }
                },
              ),
              Text(
                'Recherche',
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: numero == 2
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                  fontSize: 12,
                ),
              ),
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              IconButton(
                icon: Image.network(
                  "icons/Accueil_Favoris.svg",
                  height: 100,
                  fit: BoxFit.fill,
                  color: numero == 3
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                ),
                onPressed: () async {
                  if (numero != 3) {
                    dynamic res = await Navigator.of(context).pushNamed(
                      AppRoutes.routeFavoris,
                    );
                  }
                },
              ),
              Text(
                'Favoris',
                textAlign: TextAlign.start,
                style: TextStyle(
                  fontFamily: 'SFProDisplay',
                  fontWeight: FontWeight.bold,
                  color: numero == 3
                      ? Colors.grey
                      : Color.fromRGBO(32, 180, 108, 1),
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
