import 'dart:convert';
import 'dart:ui';

import 'package:application_flutter_musique/main.dart';
import 'package:application_flutter_musique/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Search extends StatefulWidget {
  const Search({Key? key}) : super(key: key);

  @override
  State<Search> createState() => Recherche();
}

class Recherche extends State<Search> {
  final myController = TextEditingController();
  String recherche = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(children: [
      Align(
        alignment: AlignmentDirectional(-1, -1),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(10, 0, 0, 10),
                child: Text(
                  'Rechercher',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontFamily: 'SFProDisplay',
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 30,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.04,
                  decoration: BoxDecoration(
                    color: Color(0xFFB0B0B0),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Padding(
                    padding: EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(
                          Icons.search,
                          color: Colors.black,
                          size: 24,
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsetsDirectional.fromSTEB(5, 0, 5, 0),
                            child: TextField(
                              controller: myController,
                              obscureText: false,
                              onChanged: (text) {
                                setState(() {
                                  recherche = text;
                                });
                              },
                              decoration: InputDecoration(
                                hintText: 'Rechercher',
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0x00000000),
                                    width: 1,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Color(0x00000000),
                                    width: 1,
                                  ),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(4.0),
                                    topRight: Radius.circular(4.0),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                            icon: const Icon(Icons.clear_rounded),
                            onPressed: () {
                              myController.text = "";
                              setState(() {
                                recherche = "";
                              });
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Align(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.79,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    width: 100,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.fromSTEB(
                                          10, 5, 10, 10),
                                      child: Text(
                                        'Artistes',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontFamily: 'SFProDisplay',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 30,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      FutureBuilder(
                                        future: getArtists('$recherche'),
                                        builder: (BuildContext context,
                                            AsyncSnapshot<listArtists>
                                                snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              // En attendant le résultat, affichons un loader.
                                              return new Center(
                                                  child:
                                                      new CircularProgressIndicator());
                                            case ConnectionState.done:
                                              if (snapshot.hasError) {
                                                // En cas d'erreur.
                                                return new Text(
                                                  '${snapshot.error}',
                                                  style: TextStyle(
                                                      fontFamily:
                                                          'SFProDisplay',
                                                      color: Colors.red),
                                                );
                                              } else {
                                                // Une fois le résultat obtenu, affichons la réponse obtenue.
                                                if (snapshot
                                                        .data!.liste.length >
                                                    0) {
                                                  return Column(
                                                    children: snapshot
                                                        .data!.liste
                                                        .map((single) {
                                                      return Artiste(
                                                        image: single.image,
                                                        nom: single.nom,
                                                        id: single.id,
                                                      );
                                                    }).toList(),
                                                  );
                                                } else {
                                                  return Column(children: [
                                                    Text('Aucun résultat...')
                                                  ]);
                                                }
                                              }
                                            default:
                                              return new Text(
                                                  'Erreur du chargement');
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    width: 100,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                      padding: EdgeInsetsDirectional.fromSTEB(
                                          10, 5, 10, 10),
                                      child: Text(
                                        'Albums',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontFamily: 'SFProDisplay',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 30,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: [
                                      FutureBuilder(
                                        future: getAlbums('$recherche'),
                                        builder: (BuildContext context,
                                            AsyncSnapshot<listAlbum> snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              // En attendant le résultat, affichons un loader.
                                              return new Center(
                                                  child:
                                                      new CircularProgressIndicator());
                                            case ConnectionState.done:
                                              if (snapshot.hasError) {
                                                // En cas d'erreur.
                                                return new Text(
                                                  '${snapshot.error}',
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                );
                                              } else {
                                                // Une fois le résultat obtenu, affichons la réponse obtenue.
                                                if (snapshot
                                                        .data!.liste.length >
                                                    0) {
                                                  return Column(
                                                    children: snapshot
                                                        .data!.liste
                                                        .map((single) {
                                                      return Album(
                                                        image: single.image,
                                                        nom: single.nom,
                                                        titre: single.titre,
                                                        id: single.id,
                                                      );
                                                    }).toList(),
                                                  );
                                                } else {
                                                  return Column(children: [
                                                    Text('Aucun résultat...')
                                                  ]);
                                                }
                                              }
                                            default:
                                              return new Text(
                                                  'Erreur du chargement');
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
              Align(child: new Menu(numero: 2)),
            ],
          ),
        ),
      )
    ])));
  }
}

class Artiste extends StatelessWidget {
  final String image;
  final String nom;
  final String id;
  const Artiste({
    required this.image,
    required this.id,
    Key? key,
    required this.nom,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(5, 5, 5, 5),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.08,
        decoration: BoxDecoration(
          color: Color(0xFFEEEEEE),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.15,
                height: MediaQuery.of(context).size.width * 0.15,
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.network(
                  '$image',
                ),
              ),
            ),
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: Text('$nom',
                  style: TextStyle(
                    fontFamily: 'SFProDisplay',
                  )),
            ),
            Expanded(
              child: Align(
                alignment: AlignmentDirectional(0.9, 0),
                child: IconButton(
                  icon: Icon(Icons.chevron_right),
                  onPressed: () async {
                    dynamic res = await Navigator.of(context).pushNamed(
                      AppRoutes.routeAlbums,
                      arguments: this.id,
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Album extends StatelessWidget {
  final String image;
  final String nom;
  final String titre;
  final String id;
  const Album({
    required this.image,
    Key? key,
    required this.nom,
    required this.titre,
    required this.id,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.fromSTEB(5, 5, 5, 5),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.08,
        decoration: BoxDecoration(
          color: Color(0xFFEEEEEE),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.15,
                height: MediaQuery.of(context).size.width * 0.15,
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.network(
                  '$image',
                ),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('$nom',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontFamily: 'SFProDisplay',
                    )),
                Text('$titre',
                    style: TextStyle(
                      fontFamily: 'SFProDisplay',
                    )),
              ],
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.chevron_right),
                onPressed: () async {
                  dynamic res = await Navigator.of(context).pushNamed(
                    AppRoutes.routeAlbums,
                    arguments: this.id,
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class listAlbum {
  final List<album> liste;
  listAlbum(this.liste);
}

class album {
  final String image;
  final String titre;
  final String nom;
  final String id;

  album(this.image, this.titre, this.nom, this.id);

  album.fromJson(Map<dynamic, dynamic> json)
      : image = json["strAlbumThumb"] == null ? "" : json["strAlbumThumb"],
        nom = json["strArtist"] == null ? "" : json["strArtist"],
        titre = json["strAlbum"] == null ? "" : json["strAlbum"],
        id = json["idAlbum"] == null ? "2167196" : json["idAlbum"];
}

Future<listAlbum> getAlbums(URL) async {
  if (URL != "") {
    // Construction de l'URL a appeler
    var url = Uri.parse(
        'https://theaudiodb.com/api/v1/json/523532/searchalbum.php?s=' + URL);
    // Appel
    var response = await http.get(url);
    // Convertit le json
    Map singlesMap = jsonDecode(response.body);
    // Mapper le json dans l'objet SunriseSunsetTimes
    List<album> liste = [];
    if (singlesMap["album"] != null) {
      for (int i = 0; i < singlesMap["album"].length; i++) {
        liste.add(album.fromJson(singlesMap["album"][i]));
      }
    }
    listAlbum listee = new listAlbum(liste);
    return listee;
  } else {
    List<album> liste = [];
    listAlbum listee = new listAlbum(liste);
    return listee;
  }
}

class listArtists {
  final List<artist> liste;
  listArtists(this.liste);
}

class artist {
  final String image;
  final String nom;
  final String id;

  artist(this.image, this.nom, this.id);

  artist.fromJson(Map<dynamic, dynamic> json)
      : image = json["strArtistThumb"] == null ? "" : json["strArtistThumb"],
        nom = json["strArtist"] == null ? "" : json["strArtist"],
        id = json["idArtist"] == null ? "111239" : json["idArtist"];
}

Future<listArtists> getArtists(URL) async {
  if (URL != "") {
    // Construction de l'URL a appeler
    var url = Uri.parse(
        'https://theaudiodb.com/api/v1/json/523532/search.php?s=' + URL);
    // Appel
    var response = await http.get(url);

    // Convertit le json
    Map singlesMap = jsonDecode(response.body);
    // Mapper le json dans l'objet SunriseSunsetTimes
    List<artist> liste = [];
    if (singlesMap["artists"] != null) {
      for (int i = 0; i < singlesMap["artists"].length; i++) {
        liste.add(artist.fromJson(singlesMap["artists"][i]));
      }
    }
    listArtists listee = new listArtists(liste);
    return listee;
  } else {
    List<artist> liste = [];
    listArtists listee = new listArtists(liste);
    return listee;
  }
}
