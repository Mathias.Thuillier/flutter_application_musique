import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ArtistWidget extends StatelessWidget {
  final String arg;
  const ArtistWidget({Key? key, required this.arg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(mainAxisSize: MainAxisSize.max, children: [
          Align(
            alignment: AlignmentDirectional(-1, 0),
            child: FutureBuilder(
              future: getArtist(arg),
              builder: (BuildContext context, AsyncSnapshot<Artist> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    // En attendant le résultat, affichons un loader.
                    return new Center(child: new CircularProgressIndicator());
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      // En cas d'erreur.
                      print(snapshot.error);
                      return new Text(
                        '${snapshot.error}',
                        style: TextStyle(color: Colors.red),
                      );
                    } else {
                      return Column(
                        children: snapshot.data!.list.map((item) {
                          return SingleChildScrollView(
                            child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  width: 100,
                                  height:
                                      MediaQuery.of(context).size.height * 0.4,
                                  decoration: BoxDecoration(
                                    color: Color(0xFFEEEEEE),
                                  ),
                                  child: Stack(
                                    children: [
                                      Align(
                                        alignment: AlignmentDirectional(0, 0),
                                        child: Image.network(
                                          item.thumbnail,
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              1,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Align(
                                        alignment:
                                            AlignmentDirectional(-0.90, 0.8),
                                        child: Text(
                                          item.name,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 30,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment:
                                            AlignmentDirectional(-0.90, 0.95),
                                        child: Text(
                                          '${item.country}, ${item.style}',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.blueGrey),
                                        ),
                                      ),
                                      Align(
                                        alignment:
                                            AlignmentDirectional(0.90, -0.75),
                                        child: IconButton(
                                          icon: Image.network(
                                            "icons/Like.svg",
                                            height: 100,
                                            fit: BoxFit.fill,
                                          ),
                                          onPressed: () async {
                                            print("like");
                                          },
                                        ),
                                      ),
                                      Align(
                                        alignment:
                                            AlignmentDirectional(-0.90, -0.75),
                                        child: IconButton(
                                          icon: Image.network(
                                            "icons/Fleche_gauche.svg",
                                            height: 100,
                                            fit: BoxFit.fill,
                                          ),
                                          onPressed: () async {
                                            print("back");
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsetsDirectional.fromSTEB(
                                      10, 10, 10, 0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        "${item.biographyEn.substring(0, 175)}...",
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.blueGrey),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Column(
                                            children: [
                                              FutureBuilder(
                                                future: getAlbumsList(112024),
                                                builder: (BuildContext context,
                                                    AsyncSnapshot<Albums>
                                                        snapshot) {
                                                  switch (snapshot
                                                      .connectionState) {
                                                    case ConnectionState
                                                        .waiting:
                                                      // En attendant le résultat, affichons un loader.
                                                      return new Center(
                                                          child:
                                                              new CircularProgressIndicator());
                                                    case ConnectionState.done:
                                                      if (snapshot.hasError) {
                                                        // En cas d'erreur.
                                                        print(snapshot.error);
                                                        return new Text(
                                                          '${snapshot.error}',
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.red),
                                                        );
                                                      } else {
                                                        return Column(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Padding(
                                                                padding:
                                                                    EdgeInsetsDirectional
                                                                        .fromSTEB(
                                                                            0,
                                                                            0,
                                                                            0,
                                                                            5),
                                                                child: Text(
                                                                  'Albums (${snapshot.data!.list.length})',
                                                                  style:
                                                                      TextStyle(
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        20,
                                                                  ),
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    EdgeInsetsDirectional
                                                                        .fromSTEB(
                                                                            0,
                                                                            0,
                                                                            0,
                                                                            10),
                                                                child:
                                                                    Container(
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width *
                                                                      0.95,
                                                                  height: 1,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: Colors
                                                                        .grey,
                                                                  ),
                                                                ),
                                                              ),
                                                              Column(
                                                                children: snapshot
                                                                    .data!.list
                                                                    .map(
                                                                        (item) {
                                                                  var index = snapshot
                                                                      .data!
                                                                      .list
                                                                      .indexOf(
                                                                          item);
                                                                  if (index <
                                                                      3) {
                                                                    return AlbumWidget(
                                                                      title: item
                                                                          .name,
                                                                      date: item
                                                                          .year,
                                                                      thumbnail:
                                                                          item.thumbnail,
                                                                    );
                                                                  }
                                                                  return SizedBox(
                                                                      height:
                                                                          0);
                                                                }).toList(),
                                                              ),
                                                            ]);
                                                      }
                                                    default:
                                                      return new Text('');
                                                  }
                                                },
                                              ),
                                              Column(
                                                mainAxisSize: MainAxisSize.max,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        EdgeInsetsDirectional
                                                            .fromSTEB(
                                                                0, 0, 0, 5),
                                                    child: Text(
                                                      'Titre les plus appréciés',
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                  ),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.95,
                                                    height: 1,
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                FutureBuilder(
                                  future: getTrackList(item.brainzId),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<Tracks> snapshot) {
                                    switch (snapshot.connectionState) {
                                      case ConnectionState.waiting:
                                        // En attendant le résultat, affichons un loader.
                                        return new Center(
                                            child:
                                                new CircularProgressIndicator());
                                      case ConnectionState.done:
                                        if (snapshot.hasError) {
                                          // En cas d'erreur.
                                          print(snapshot.error);
                                          return new Text(
                                            '${snapshot.error}',
                                            style: TextStyle(color: Colors.red),
                                          );
                                        } else {
                                          return Column(
                                              children: snapshot.data!.list
                                                  .map((track) {
                                            var index = snapshot.data!.list
                                                    .indexOf(track) +
                                                1;
                                            return Title(
                                              title: track.title,
                                              index: index.toString(),
                                            );
                                          }).toList());
                                        }
                                      default:
                                        return new Text('');
                                    }
                                  },
                                ),
                              ],
                            ),
                          );
                        }).toList(),
                      );
                    }
                  default:
                    return new Text('');
                }
              },
            ),
          )
        ] // Generated code for this Column Widget...
            ),
      ),
    );
  }
}

class Artist {
  Artist(this.list);

  final List<ArtistElement> list;

  // Artist.fromJson(Map<dynamic, dynamic> json) :  artists = json["artists"] == null ? null : List<ArtistElement>.from(json["artists"].map((x) => ArtistElement.fromJson(x)));
  getArtistList() {
    return list;
  }
}

class ArtistElement {
  ArtistElement(
    this.name,
    this.style,
    this.biographyEn,
    this.biographyFr,
    this.country,
    this.thumbnail,
    this.brainzId,
  );

  final String name;
  final String style;
  final String biographyEn;
  final String biographyFr;
  final String country;
  final String thumbnail;
  final String brainzId;

  ArtistElement.fromJson(Map<String, dynamic> json)
      : name = json["strArtist"] == null ? null : json["strArtist"],
        style = json["strStyle"] == null ? null : json["strStyle"],
        biographyEn =
            json["strBiographyEN"] == null ? null : json["strBiographyEN"],
        biographyFr =
            json["strBiographyFR"] == null ? null : json["strBiographyFR"],
        country = json["strCountry"] == null ? null : json["strCountry"],
        thumbnail =
            json["strArtistThumb"] == null ? null : json["strArtistThumb"],
        brainzId =
            json["strMusicBrainzID"] == null ? null : json["strMusicBrainzID"];
}

Future<Artist> getArtist(id) async {
  print(id);
  var url =
      Uri.parse("https://www.theaudiodb.com/api/v1/json/2/artist.php?i=$id");
  var response = await http.get(url);
  Map artistMap = jsonDecode(response.body);
  List<ArtistElement> liste = [];
  for (int i = 0; i < artistMap["artists"].length; i++) {
    liste.add(ArtistElement.fromJson(artistMap["artists"][i]));
  }
  Artist listArtist = Artist(liste);
  return listArtist;
}

class Albums {
  Albums(this.list);

  final List<AlbumElement> list;

  getAlbumsList() {
    return list;
  }
}

class AlbumElement {
  AlbumElement(this.name, this.year, this.thumbnail);

  final String name;
  final String year;
  final String thumbnail;

  AlbumElement.fromJson(Map<String, dynamic> json)
      : name = json["strAlbum"] == null ? null : json["strAlbum"],
        year = json["intYearReleased"] == null ? null : json["intYearReleased"],
        thumbnail =
            json["strAlbumThumb"] == null ? null : json["strAlbumThumb"];
}

Future<Albums> getAlbumsList(id) async {
  var url = Uri.parse(
      "https://www.theaudiodb.com/api/v1/json/523532/album.php?i=$id");
  var response = await http.get(url);
  Map albumMap = jsonDecode(response.body);
  List<AlbumElement> liste = [];
  for (int i = 0; i < albumMap["album"].length; i++) {
    liste.add(AlbumElement.fromJson(albumMap["album"][i]));
  }
  Albums listArtist = Albums(liste);
  return listArtist;
}

class Tracks {
  Tracks(
    this.list,
  );

  final List<Track> list;
  getTrackList() {
    return list;
  }
}

class Track {
  Track(
    this.title,
  );

  final String title;

  Track.fromJson(Map<String, dynamic> json)
      : title = json["strTrack"] == null ? null : json["strTrack"];
}

Future<Tracks> getTrackList(id) async {
  print(id);
  var url = Uri.parse(
      "https://theaudiodb.com/api/v1/json/523532/track-top10-mb.php?s=$id");
  var response = await http.get(url);
  Map trackMap = jsonDecode(response.body);
  List<Track> liste = [];
  for (int i = 0; i < trackMap["track"].length; i++) {
    liste.add(Track.fromJson(trackMap["track"][i]));
  }
  Tracks listTrack = Tracks(liste);
  return listTrack;
}

class AlbumWidget extends StatelessWidget {
  final String title;
  final String date;
  final String thumbnail;

  const AlbumWidget(
      {required this.title,
      required this.date,
      required this.thumbnail,
      Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        width: MediaQuery.of(context).size.width * 0.95,
        height: 100,
        decoration: BoxDecoration(
          color: Color(0xFFD6D6D6),
        ),
        child: Padding(
          padding: EdgeInsetsDirectional.fromSTEB(10, 0, 10, 0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 75,
                height: 75,
                decoration: BoxDecoration(
                  color: Color(0x00EEEEEE),
                ),
                child: Image.network(
                  thumbnail,
                  width: 50,
                  height: 100,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.6,
                height: 100,
                decoration: BoxDecoration(
                  color: Color(0x00EEEEEE),
                ),
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(20, 20, 20, 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        textAlign: TextAlign.start,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        date,
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
              IconButton(
                icon: Image.network(
                  "icons/Like.svg",
                  height: 100,
                  fit: BoxFit.fill,
                ),
                onPressed: () async {
                  print("like");
                },
              ),
            ],
          ),
        ),
      ),
      SizedBox(height: 10),
    ]);
  }
}

class Title extends StatelessWidget {
  final String title;
  final String index;
  const Title({required this.title, required this.index, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.95,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(5, 5, 20, 5),
                child: Text(
                  index,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsetsDirectional.fromSTEB(5, 5, 20, 5),
                child: Text(
                  title,
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.95,
            height: 1,
            decoration: BoxDecoration(
              color: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }
}
