// routes.dart
import 'package:application_flutter_musique/albums.dart';
import 'package:application_flutter_musique/artist.dart';
import 'package:application_flutter_musique/favoris.dart';
import 'package:application_flutter_musique/recherche.dart';
import 'package:flutter/material.dart';

import 'main.dart';

class AppRoutes {
  static const String routeHome = '/';
  static const String routeRecherche = '/p2';
  static const String routeFavoris = '/p3';
  static const String routeAlbums = '/albums';
  static const String routeArtiste = '/albums';

  const AppRoutes._();

  static Route<dynamic> getRoutes(RouteSettings settings) {
    WidgetBuilder builder;

    Uri route = Uri.parse(settings.name!);
    String routeName = settings.name!;
    dynamic arguments = settings.arguments;

    switch (routeName) {
      case routeHome:
        builder = (BuildContext context) => const Screen1();
        break;
      case routeRecherche:
        builder = (BuildContext context) => Search();
        break;
      case routeFavoris:
        builder = (BuildContext context) => Favoris();
        break;
      case routeAlbums:
        builder = (BuildContext context) => AlbumPageWidgetState(
              arg: arguments,
            );
        break;
      case routeArtiste:
        builder = (BuildContext context) => ArtistWidget(arg: arguments);
        break;
      default:
        throw Exception('');
    }

    return MaterialPageRoute<dynamic>(
      builder: builder,
      settings: settings,
    );
  }
}
